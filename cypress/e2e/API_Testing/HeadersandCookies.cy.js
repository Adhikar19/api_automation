// Describe block to group your API testing-related tests and hooks
describe("API testing", () => {
    let authenticationToken = null;
  
    before("Creating Access token", () => {
      cy.request({
        method: 'POST',
        url: 'https://simple-books-api.glitch.me/api-clients',
        headers: {
          'Content-Type': 'application/json'
        },
        body: {
          clientName: 'Ramesh Ramesh',
          clientEmail: Math.random().toString(36).substring(2) + "@gmail.com"
        }
      }).then(response => {
        authenticationToken = response.body.accessToken;
      });
    });
  
    // Test case to make an authenticated API request with Headers and Bearer Token
    it("should make an authenticated API request with Headers and Bearer Token", () => {
      cy.request({
        method: 'GET',
        url: 'https://your-api-endpoint.com/data',
        headers: {
          // Adding the Bearer token authentication header
          Authorization: `Bearer ${authenticationToken}`,
          // Adding custom headers if needed
          'Custom-Header': 'Custom-Value'
        },
        // add cookies
        cookies: [{ name: 'accessToken', value: authenticationToken }]
      }).then(response => {
        expect(response.status).to.eq(200);
       
      });
    });
  
    // Test case to make an authenticated API request with Cookies
    it("should make an authenticated API request with Cookies", () => {
      cy.request({
        method: 'GET',
        url: 'https://your-api-endpoint.com/data',
        // Adding cookies 
        cookies: [{ name: 'accessToken', value: authenticationToken }]
      }).then(response => {
        expect(response.status).to.eq(200);
        
      });
    });
  
    
  
  });
  