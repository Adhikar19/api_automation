describe("Parsing Json response", () => {

    // Positive Test
    it("Parsing simple json function", () => {
        cy.request({
            method: 'GET',
            url: "https://fakestoreapi.com/products",
        })
        .then((response) => {
            expect(response.status).to.equal(200);
            expect(response.body[0].id).to.equal(1);
            expect(response.body[0].title).to.equal("Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops");
            expect(response.body[0].price).to.equal(109.95);
            expect(response.body[0].rating.rate).to.equal(3.9);
        });
    });

    // Negative Test
    it("Parsing with incorrect properties", () => {
        cy.request({
            method: 'GET',
            url: "https://fakestoreapi.com/products",
        })
        .then((response) => {
            expect(response.status).to.not.equal(404); // Expecting that the status isn't 404
            expect(response.body[0].id).to.not.equal(1000); // Assuming 1000 is an ID that shouldn't exist
            expect(response.body[0].title).to.not.be.empty; // Expecting title not to be empty
            expect(response.body[0].price).to.not.be.below(0); // Price shouldn't be negative
            expect(response.body[0].rating.rate).to.not.be.above(5); // Assuming rating cannot be more than 5
        });
    });

    // Negative Test: Non-existent Endpoint
    it("Should return 404 for non-existent endpoint", () => {
        cy.request({
            method: 'GET',
            url: "https://fakestoreapi.com/nonexistent-endpoint",
            failOnStatusCode: false
        })
        .then((response) => {
            expect(response.status).to.equal(404);
        });
    });
    
    // Negative Test: Timeout
    it("Should respond in a timely manner", () => {
        cy.request({
            method: 'GET',
            url: "https://fakestoreapi.com/products",
            timeout: 5000
        })
        .then((response) => {
            expect(response.duration).to.be.below(5000);
        });
    });

});
