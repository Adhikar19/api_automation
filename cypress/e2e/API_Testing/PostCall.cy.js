// Assuming you are using Cypress for testing

describe("This is for the Post calls", () => {

    // Base URL for your requests
    const baseUrl = "https://dummyapi.io/data/v1/user/create";

    // Common headers for each request
    const headers = {
        'Content-Type': 'application/json',
        'app-id': '64bcfda2a2599c46d0c7d243'
    };

    it("Hard code data with unique email", () => {
        // Generate a unique email address by appending the current timestamp
        const uniqueEmail = `adhikar${Date.now()}@yopmail.com`;

        const requestBody = {
            firstName: "Adhikar chaudhary testing",
            lastName: "Chaudhary testing",
            email: uniqueEmail  // Using the unique email here
        };

        cy.request({
            method: "POST",
            url: baseUrl,
            body: requestBody,
            headers: headers
        })
        .then((response) => {
            expect(response.status).to.eq(200);  // Expecting 200 OK status code here
            expect(response.body.firstName).to.eq("Adhikar chaudhary testing");
            expect(response.body.lastName).to.eq("Chaudhary testing");
            expect(response.body.email).to.eq(uniqueEmail); 
        });
    });

    it("Dynamic type approachs 2", () => {
        const requestBody = {
            firstName: Math.random().toString(5).substring(2),
            lastName: Math.random().toString(5).substring(2),
            email: `${Math.random().toString(5).substring(2)}@yopmail.com`
        };

        cy.request({
            method: "POST",
            url: baseUrl,
            body: requestBody,
            headers: headers
        })
        .then((response) => {
            expect(response.status).to.eq(200);
            expect(response.body.firstName).to.eq(requestBody.firstName);
            expect(response.body.lastName).to.eq(requestBody.lastName);
            expect(response.body.email).to.eq(requestBody.email); 
        });
    });

    it("Using fixture", () => {
        // Ensure your fixture data (student_data.json) is in cypress/fixtures directory
        cy.fixture('student_data').then((data) => {
            const requestBody = data;

            cy.request({
                method: "POST",
                url: baseUrl,
                body: requestBody,
                headers: headers
            })
            .then((response) => {
                expect(response.status).to.eq(200);
                expect(response.body.firstName).to.eq(requestBody.firstName);
                expect(response.body.lastName).to.eq(requestBody.lastName);
                expect(response.body.email).to.eq(requestBody.email); 
            });
        });
    });
});
