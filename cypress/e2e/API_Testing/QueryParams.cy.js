

describe("API Testing", () => {
    it("Passing Query through parameters", () => {
        cy.request({
            method: 'GET',
            url: 'https://reqres.in/api/users',
            qs: { page: 2 }  
        })
        .then((response) => {
            // Check status code
            expect(response.status).to.eq(200);

            // Check returned data
            expect(response.body.page).to.eq(2);
            expect(response.body.data).to.have.length(6);

            // Check properties of the first item in the returned data array
            expect(response.body.data[0]).to.have.property('id', 7);
            expect(response.body.data[0]).to.have.property('first_name', 'Michael');
        });
    });
});
